---
layout: markdown_page
title: "E-group KPIs"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## What are KPIs

Every department at GitLab has Key Performance Indicators (KPIs).
The term "metric" can mean multiple things; it can be a definition, a goal, or the actual results.
Avoid the term metric where we can be more explicit. Use KPI instead.
A department's KPIs are owned by the respective member of e-group.
The goals are merged by the CEO.

The KPI definition should be in the most relevant part of the handbook which is organized by [function and results](/handbook/handbook-usage/#style-guide-and-information-architecture).
In the definition, it should mention what the canonical source is for this indicator.
Where there are formulas include those as well.
Goals related to KPIs should co-exist with the definition.
For example, "Wider community contributions per release" should be in the Community Relations part of the handbook and "Average days to hire" should be in the Recruiting part of the handbook.

If a metric is not defined, create an MR to propose a definition. Ask the appropriate business stakeholder to review and merge.

The data team maintains [a list of GitLab KPIs and links to where they are defined](/handbook/business-ops/data-team/metrics/index.html.md).

## Public

In the doc 'GitLab Metrics at IPO' are the KPIs that we may share publicly.
The [Corporate Metrics Dashboard](https://app.periscopedata.com/app/gitlab/409920/WIP:-Corporate-Metrics) can be found in [Periscope](/handbook/business-ops/data-team/periscope-directory/).

## GitLab KPIs

GitLab goals are duplicates of goals of the reports.
GitLab goals are the most important indicators of company performance.

The [GitLab KPIs Dashboard](https://app.periscopedata.com/app/gitlab/434327/WIP:-Corporate-KPIs) can be found in [Periscope](/handbook/business-ops/data-team/periscope-directory/).
