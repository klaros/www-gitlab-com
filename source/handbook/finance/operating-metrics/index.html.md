---
layout: markdown_page
title: "Operating Metrics"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Monthly KPI and OKR Review

### Purpose

For each [executive](/company/team/structure/#executives) we have a monthly call to discuss the metrics of that department in order to:

1. Makes it much easier to stay up to date for everyone.
1. Be accountable to the rest of the company.
1. Understand month to month variances.
1. Understand against the plan, forecast and operating model.
1. Ensure there is tight connection between OKRs and KPIs.

Some executives will have additional calls in areas that report to them based on the number and importance of metrics associated with the function.

### Metric

1. [KPIs](/handbook/ceo/kpis/) of that department
1. [OKRs](/company/okrs/) that are assigned to this executive.
1. Corporate metrics sheet (need link)
1. Operating Model (need link)

### Agenda

1. Review KPIs and conclude on implications for operating model.
1. Review status of current quarter OKRs.
1. Discuss proposals for different measurement.
1. Determine if external benchmarks are required.
1. Discuss proposals for addition of new KPIs.
1. Discuss proposals for deprecation of existing KPIs.
1. Review decisions & action items.

### Timing

Meetings are monthly starting on the 10th day after month end.

### Invitees

Required invites are the executive and the CFO. Optional attendees are the rest of the e-team and anyone who has an interest in the metric.

### Meeting Format

1. The functional owner will prepare a google slide presentation with the content to be reviewed.
1. The finance business partner assigned to the functional area will meet with the owner at least one week in advance and ensure that follow-ups from last meeting have been completed and that data to be presented has proper definitions and is derived from a Single Source of Truth.
1. The title of every slide should be the key takeaway
1. A label on the slide should convey whether the metric result is "on-track" (green), "needs improvement" (yellow), or is an "urgent concern" (red).
1. A google doc will also be linked from the calendar invite for participants to log questions or comments for discussion, and to any additional track decisions & action items.
1. Wherever possible the metric being reviewed should be compared to Plan, OKR target, KPI target, or industry benchmark.
1. The functional owner is expected to present a summary of highlights which should not last more than three minutes. A pre-recorded video can be an efficient way to do this.
1. The functional owner is responsible for preparing the document 24 hours advance of the meeting. The owner should update the meeting invite and send to all guests so they know the materials are ready for review.
1. A [blank template](https://docs.google.com/presentation/d/1lfQMEdSDc_jhZOdQ-TyoL6YQNg5Wo7s3F3m8Zi9NczI/edit) still needs labels

### Future

We want to get from Google Sheets to reviewing a live dashboard.

<div class="alert alert-purple center">
  <p class="purple center" style="font-size: 34px ; text-align: center ; margin: auto">
    <strong>We are actively working to move away from the methodology of maintaining one Operating Metrics page.</strong>
  </p>
</div>

**Please do not update this section. Instead, please define a metric in-place, where it would make the most sense for that metrics to live in the handbook. For example, the Average Days to Hire metrics should live within the recruiting section of the Handbook.**

For more details, see Emilie Schario, Data Analyst, and Sid Sijbrandij, CEO, discuss the best way to organize metrics definitions in the company handbook. Progress updates can be found in [gitlab-data#1241](https://gitlab.com/gitlab-data/analytics/issues/1241).

<iframe width="560" height="315" src="https://www.youtube.com/embed/T4fQp9jtKWU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Here's another follow up discussion that includes Joe Davidson, SDR, and brings additional clarity around addressing the ownership question.

<iframe width="560" height="315" src="https://www.youtube.com/embed/8PZeorUJEbE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## Defined Metrics

## Alliances

## Engineering

### Customer Satisfaction (CSAT)
{: #csat}
A measure of the satisfaction of service from a customer's interaction with the GitLab Support team. Based on survey responses from customers after the ticket is solved by the GitLab Support team using a Good/Bad rating.

```
(CSAT = Satisfied [total good scores]  / Total Survey Responses [good scores + bad scores])
```

This calculation excludes cases where a survey was not offered or where it was offered but no score was provided.

### Service Level Agreement (SLA)
GitLab Support commits to an initial substantive response in a specified amount of time from the time the customer submits a ticket.  The SLA for this first reply is based on a customer's Support plan.  The SLA is currently measured on tickets submitted by customers with our top Support plans (Premium for Self-managed, Gold for Gitlab.com). The SLA is calculated by (Number of Times SLA met / Total Tickets SLA was applicable).


## Finance
### Average days to close
The number of business days required to report final monthly financial results.  
As a private company our target is 10 business days moving to 5 business days as a public company.

### Capital Consumption
TCV less Total Operating Expenses.  
This metric tracks net cash consumed excluding changes in working capital (i.e. burn due to balance sheet growth).
Since the growth in receivables can be financed with using cheap debt instead of equity is a better measure of capital efficiency than cash burn.

### Cash Burn, Average Cash Burn and Runway
The change in cash balance from period to period excluding equity or debt financing. Average cash burn is measured over the prior three months. Runway is defined as the number of months based on cash balance plus available credit divided by average cash burn. Our target is that this metric is always greater than 12 months.

### Credit
Lost or lowered contract value that occurs before a subscription renewal or subscription cancellation

### Customers
We define customers in the following categorical level of detail:
1. Subscription: A unique subscription contract with GitLab for which the term has not ended. As customers become more sophisticated users of GitLab the number of subscriptions may decline over time as Accounts and Parents consolidate subscriptions to gain more productivity.
1. Account: An organization that controls multiple subscriptions that have been purchased under a group with  common leadership. In the case of the U.S. government, we count U.S. government departments and major agencies as a unique account.
1. Parent: An accumulation of Accounts under an organization with common ownership. In the case of the U.S. government, we count U.S. government major agencies as a unique parent account. (In Salesforce this is the `Ultimate Parent Account` field)

Because "customer" can have three different meanings whenever customer is used in presenting data it must be qualified by the type of customer. The default description is parent. When the default is used no further description is required. When account or subscription is being reported then the title or field description on the chart must be added to call out the basis for reporting. Metrics that are based on customer data should also carry a clarifying description.

#### Customer Segmentation
Customer segmentation follows the segmentation as laid out in the [Business Operations Handbook](https://about.gitlab.com/handbook/business-ops/#segmentation) at the [Parent Account level](#customers).

#### Customer Counts
1. Subscriptions: Given that subscriptions can consolidate, fan out, be renewed, and experience other kinds of transformations over time, counting subscriptions are less straightforward than counting accounts. The core principle is: if a subscription was active at any point in time in the proposed timeframe, it is counted as active.

1. Accounts and Parents: If an account was active at any point in time during the proposed timeframe it is counted as active. For example, an account that is active from March 2019 to May 2019 but is inactive from June 2019-on is counted for CY2019, FY2020 (which runs from February 2019-January 2020), 2020-Q1, and 2020-Q2; it is not counted in 2020-Q3 or 2020-Q4.

<details>
<summary>Specific Examples of Subscription Counts (Click to expand)</summary>

<ul>

<li> Non-renewal: A subscription that is active from March 2019 to May 2019 but is inactive from June 2019-on is counted for CY19, FY20 (which runs from February 2019-January 2020), FY20-Q1 (Feb-April 2019), and FY20-Q3 (May-July 2019); it is not counted in FY20-Q3 or FY20-Q4. </li>
<li> Standard renewal:  A subscription that is active from March 2019 to May 2019 and is renewed in June 2019 with a single subscription will have a total number of 1 subscriptions at all points in which it is counted. </li>
<li> Consolidation: Two subscriptions are active under one account from March 2019 to May 2019. In June 2019, they are consolidated into one subscription. (The use of "consolidation" does not imply a smaller subscription, just that there are now fewer subscriptions.) In April 2019, the count of active subscriptions for that month will be 2 subscriptions. In July 2019, the count of active subscriptions for that month with be 1 subscription; at the same time, in July 2019, the count of active subscriptions for the month of April 2019 will be updated to reflect 1 given the consolidation. Once subscriptions are consolidated, they will count as 1. The historical count of subscriptions will go down as subscriptions are consolidated. </li>
<li> Fan out: One subscription is active under one account from March 2019 to May 2019. In June 2019, these are cancelled and renewed to two new subscriptions. In April 2019, the count of active subscriptions for that month will be 1 subscription. In July 2019, the count of active subscriptions for that month with be also be 1 subscription. For all periods of time, these subscriptions will count as one. </li>
</ul>

This method of counting subscriptions may understate the number of active subscriptions active at any given point in time. This approach to counting reduces complexity and scale, makes clear we are never overstating subscriptions, and makes the counting process straightforward.

</details>

### Days Sales Outstanding (DSO)
Average Accounts Receivable balance over prior 3 months divided by Total Contract Value (TCV) bookings over the same period multipied by 90 that provides an average number of days that customers pay their invoices.  Link to a good [definition](https://www.investopedia.com/terms/d/dso.asp)  and [Industry guidance](https://www.opexengine.com/software-industry-revenue-growth-accelerating-and-hiring-expected-to-jump-according-to-new-siiaopexengine-report/) suggests the median DSO for SAAS companies is 76 days. Our target at GitLab is 45 days.

### Free Cash Flow (FCF)
{: #fcf}
Cash flow from operations as defined by GAAP less Capital Expenditures.

### Gross Burn Rate
Total operating expenses plus capital expenditures.

### Gross Margin
{: #gross-margin}
Total revenue less cost of revenues as defined by GAAP and reported in the Company's financial statements.  Gross margin targets are 90% for self managed. 80% for gitlab.com (SaaS) and 35% for professional services.

### Non GAAP Revenue (Ratable Recognition)
The amount of subscription revenue recognized using ratable accounting treatment as calculated by the subscription amount divided equally over the subscription term. Note that other GAAP adjustments such as non-delivery, performance obligations are not accounted for in this metric.

### Reasons for Churn / Expansion, Dollar Weighted
A measure of the causes for retention (compared to the same time period for the previous year) MRR decreases (churn) or increases (expansion). Churn is specified as Cancellation or Downgrades. Expansion is specified as Seat Expansion, Product Change, Product Change/Seat Change Mix, or Discount/Price Change. These are reported as a percentage using the change in MRR for the given reason over the total MRR change for all types in either the Churn or Expansion category. Trueups are excluded from these metrics.

### Retention, Gross & Net (Dollar Weighted)
We measure Net and Gross Retention aggregated by month, for the three levels of customers described above.

    Gross Retention (%) = (min(b, a) / a) * 100%

    Net Retention (%) = (b / a) * 100%

    a. MRR 12 months ago, from currently active customers
    b. Current MRR from the same set of customers as a.

Gross Retention cannot exceed 100%. [Industry guidance]("http://www.forentrepreneurs.com/saas-metrics-2/") suggests median gross dollar churn performance for SaaS/subscription companies is 8% per year (or 92% gross retention).

Note that since MRR values can change on a regular basis, retention can therefore change since it relies on MRR. See [Monthly Recurring Revenue](/handbook/finance/operating-metrics/#monthly-recurring-revenue-mrr) for more details

### Revenue

#### Annual Recurring Revenue (ARR)
MRR times 12

#### ARR by Annual Cohort
{: #arr-cohort}
ARR can be sliced many different ways for analysis. In the ARR by Cohort analyses, we look at ARR (as defined above) by the Fiscal Year Cohort. That analysis can be found on the [Retention Dashboard](https://app.periscopedata.com/app/gitlab/403244/Retention).

#### Monthly Recurring Revenue (MRR)
Monthly recurring revenue from subscriptions that are active on the last day of the month plus (true-ups/12).

Subscription data from Zuora is the sole source of tracked MRR. The MRR value for a given month is based on the rate plan charge that is active on the last day of the month. True-up revenue is divided by twelve and added to the subscription MRR for the month it was charged.

Note that MRR values can change on a regular basis. The primary causes are customers updating, renewing, or canceling their subscriptions in a month different from when the original subscription ended. Updates increase and decrease the MRR values for all previous months of a subscription. Renewals increase MRR for all months since the start of the subscription. Cancellations decrease MRR for all months the subscription was active.


## Marketing

### Cost per MQL
Marketing expense divided by the number of MQLs

### Marketing efficiency ratio
IACV / marketing spend

### Marketo Qualified Lead (MQL)
{: #mql}
[Marketo Qualified Lead](/handbook/business-ops/#customer-lifecycle)

### Social Response Time
{: #social-response-time}
 [Community Response Channels](/handbook/marketing/community-relations/community-advocacy/#community-response-channels)


## People ops

## Product
### GitLab.com User and Group Churn
A GitLab.com user or group is considered churned if they were active in a given month but not active in the following month. Also see definitions for an [active user](https://about.gitlab.com/handbook/finance/operating-metrics/#monthly-active-user-mau) and an [active group](https://about.gitlab.com/handbook/finance/operating-metrics/#monthly-active-group-mag). We also measure gitlab.com churn and retention on a customer basis as per our definitions of customer.

### Lost instances
A lost instance of self-managed GitLab didn't send a usage ping in the given month but it was active in the previous month.

### Monthly Active Group (MAG)
A GitLab.com group with at least 1 project with at least 1 [Event](https://docs.gitlab.com/ee/api/events.html) in a calendar month.

### Monthly Active User (MAU)
A GitLab.com user with at least 1 [Audit Event](https://docs.gitlab.com/ee/administration/audit_events.html) in a calendar month.


## Sales

### Average Sales Price (ASP)
IACV per won deal.

### Contract Value

#### Annual Contract Value (ACV)
Current Period subscription bookings which will result in revenue over next 12 months. For multiple year deals with contracted ramps, the ACV will be the average annual booking per year.

#### Incremental Annual Contract Value (IACV)
Value of new bookings from new and existing customers that will result in recurring revenue over the next 12 months less any credits, lost renewals, downgrades or any other decrease to annual recurring revenue. Excluded from IACV are bookings that are non-recurring such as professional services, training and non-recurring engineering fees (PCV). Also equals ACV less renewals. However, bookings related to true-up licenses, although non-recurring, are included in IACV because the source of the true-ups are additional users which will result in recurring revenue.
IACV may relate to future periods (within twelve months).

Beg ARR + IACV may not equal ending ARR due to the following reasons:
1. Timing difference due to IACV that will not start until a later period.
1. ARR will be reduced by subscriptions that have expired but which may be recorded as a reduction to IACV in a different period (either earlier or later).

#### Gross Incremental Annual Contract Value (Gross IACV)
Value of new bookings from new and existing customers that will result in recurring revenue over the next 12 months. Gross IACV includes true-ups and refunds.

#### Growth Incremental Annual Contract Value (Growth IACV)
Contract value that increases at the time of subscription renewal

#### New Incremental Annual Contract Value (New IACV)
Contract value from a new subscription customer

#### ProServe Contract Value (PCV)
{: #pcv}
Contract value that is not considered a subscription and the work is performed by the Professional Services team

#### Total Contract Value (TCV)

### Customer Acquisition Cost (CAC)
Total Sales & Marketing Expense/Number of New Customers Acquired

### Customer Acquisition Cost (CAC) Ratio
{: #cac-ratio}
Total Sales & Marketing Expense/ACV from new customers (excludes growth from existing).  [Industry guidance](http://www.forentrepreneurs.com/2017-saas-survey-part-1/) reports that median performance is 1.15 with anything less than 1.0 being considered very good.
All bookings in period (including multiyear); bookings is equal to billings with standard payment terms.

### Downgrade
Contract value that results in a lower value than the previous contract value. Downgrade examples include seat reductions, product downgrades, discounts, and customers switching to Reseller at time of renewal.

### Field efficiency ratio
IACV / sales spend

### Licensed Users
{: #licensed-users}
Number of contracted users on active paid subscriptions. Excludes OSS, Education, Core and other non-paid users. Data source is Zuora.

### Life-Time Value (LTV)
{: #ltv}
Customer Life-Time Value = Average Revenue per Year x Gross Margin% x 1/(1-K) + GxK/(1-K)^2; K = (1-Net Churn) x (1-Discount Rate).  GitLab assumes a 10% cost of capital based on current cash usage and borrowing costs.

### Life-Time Value to Customer Acquisition Cost Ratio (LTV:CAC)
{: #ltv-to-cac-ratio}
The customer Life-Time Value to Customer Acquisition Cost ratio (LTV:CAC) measures the relationship between the lifetime value of a customer and the cost of acquiring that customer. [A good LTV to CAC ratio is considered to be > 3.0.](https://www.klipfolio.com/resources/kpi-examples/saas-metrics/customer-lifetime-value-to-customer-acquisition-ratio)

### Lost Renewal
Contract value that is lost at the time of subscription renewals. Lost Renewals examples include cancellations at or before the subscription renewal date.

### Magic Number
IACV for trailing three months / Sales & Marketing Spend over trailing months -6 to months -4 (one quarter lag). [Industry guidance](http://www.thesaascfo.com/calculate-saas-magic-number/) suggests a good Magic Number is > 1.0.

### New ACV / New Customers
Net IACV that come from New Customers divided by the number of net closed deals in the current month.

### New ACV / New Customers by Sales Assisted
Net IACV that come from New Customers and sold by the field sales team divided by the number of net closed deals in the current month.

### Revenue per Licensed User (also known as ARPU)

ARR divided by number of [Licensed Users](https://about.gitlab.com/handbook/finance/operating-metrics/#licensed-users)

### Sales Efficiency Ratio
IACV / sales and marketing spend. [Industry guidance](http://tomtunguz.com/magic-numbers/) suggests that average performance is 0.8 with anything greater than 1.0 being considered very good.

### Sales Qualified Lead (SQL)
[Sales Qualified Lead](/handbook/business-ops/#customer-lifecycle)

### Rep Productivity
{: #rep-productivity}
Monthly IACV * 12 / number of native quota-carrying sales reps
