---
layout: markdown_page
title: "IT Ops"
---
# Welcome to the IT Ops Handbook

The IT Ops department is part of the [GitLab Business Ops](/handbook/business-ops) function that guides systems, workflows and processes and is a singular reference point for operational management.

## GET IN TOUCH

* [IT Ops Issues](https://gitlab.com/gitlab-com/business-ops/itops/issue-tracker/issues)
* #it-ops on slack

## On this page
{:.no_toc}

- TOC
{:toc}

## Mission Statement

IT Ops will work with Security, PeopleOps and Business Operations to develop automated on-boarding and off-boarding processes. We will develop secure integrations between Enterprise Business Systems and with our Data Lake. We will develop tooling and process to facilitate end-user asset management, provisioning and tracking. We will work to build API Integrations from the HRIS to third party systems and GitLab.com. We triage IT related questions as they arise. We build and maintain cross-functional relationships with internal teams to champion initiatives. We will spearhead on-boarding and off-boarding automation efforts with a variety of custom API integrations, including GitLab.com and third-party resources, not limited to our tech-stack, with scalability in mind.


## Access requests

For information about the access request process, please refer to the [access request project](
https://gitlab.com/gitlab-com/access-requests).

### Baseline Entitlements

For all incoming GitLabbers, access to systems is handled through on-boarding issues. If your on-boarding issue is complete and closed but you still need access to a system listed below, please create a [new access request issue](https://gitlab.com/gitlab-com/access-requests/issues/new) and add a link to this page in lieu of manager approval:

#### Systems that all GitLabbers should have access to

| System Name | Business Purpose | [Data Classification](https://about.gitlab.com/handbook/engineering/security/data-classification-policy.html) |
|---|---|---|
| 1Password | User Password Management | RED |
| Expensify | Expense Claims and Management | ORANGE |
| Greenhouse | Recruiting Portal | RED |
| Moo | Business Cards | YELLOW |
| NexTravel | Travel booking | ORANGE |
| Periscope | Data Visualisation | ORANGE |
| Sertifi | Securely and swiftly collect eSignatures, payments, and authorizations digitally | YELLOW |
| Zoom | For video conferenicing / meetings | RED |

## Automated Group Membership Reports for Managers

If you would like to check whether or not a GitLabber is a member of a Slack or a G-Suite group, you can view the following automated group membership reports:

[G-Suite Group Membership Reports](https://gitlab.com/gitlab-com/security-tools/report-gsuite-group-members)

[Slack Group Membership Reports](https://gitlab.com/gitlab-com/security-tools/report-slack-group-members)

## Two factor issues and requests for support

We are Helpful -- not a Helpdesk.  We are well positioned to provide support to all teams, but our focus and preference is always on larger projects.  We ask that all efforts be made to self-resolve before contacting IT-Ops for support.

Certain support items like 2FA resets are time sensitive and are given the highest priority.

As a distributed team, our current standard coverage window is ~13:00-22:00 UTC. High volumes of issues being triaged can dictate the delay in response within that window.  If the issue is extremely time sensitive and warrants escalation, use judgement on whether or not it can wait until ‘business hours’. Contact details can be found in slack for escalation.

### How to contact us, or escalate priority issues outside of standard hours:

Team members contact details can be found in slack profiles.

Slack is not ideal for managing priorities of incoming issues, so we ask that all such requests get sent to it-issues@ or create an issue at [IT Ops Issues](https://gitlab.com/gitlab-com/business-ops/itops/issue-tracker/issues) and we will triage and address them as soon as we can.  All issues created in the servicedesk queue are public by default.

Privileged or private communications should be sent to itops@ where all new issues are private by default, visible only to the reporter and appropriate team members. 

Screenshots and videos are very helpful when experiencing an issue, especially if there is an error message.

## Laptops

### New Laptops

Laptops are purchased by IT Ops when a GitLabber comes on board; the GitLabber will be sent a form to fill out for ordering.

### Laptop Refresh

Replacement laptops for broken GitLab laptops can be purchased as needed by [creating an issue](https://gitlab.com/gitlab-com/business-ops/itops/issue-tracker/issues/new?issue) in the IT Ops issue tracker project and using the `repair_replace` template.

This process can also be followed for laptops that are not broken but old enough that you are having trouble completing your work. Please refer to the [spirit of spending company money](https://about.gitlab.com/handbook/spending-company-money/) when deciding whether or not it is appropriate to replace your functioning laptop. Everyone's needs are different so it is hard to set a clear timeline of when computer upgrades are necessary for all employees, but GitLabbers become eligible for an updated laptop after 3 years.

### Configuring New Laptops

New laptops should be configured with security in mind. Please refer to [security best practices](https://about.gitlab.com/handbook/security/#best-practices) when configuring new laptops. **All GitLabbers must provide proof of whole-disk encryption within the new laptop order issue.**

### Returning Old Laptops

Part of the IT Ops replacement laptop process is providing each GitLabber with instructions about how to return their old laptop (whether outdated or broken). All laptops must be returned **within 2 weeks of receiving the replacement laptop**, so please prioritize transferring information between laptops within this timeframe.

All GitLabber laptops must be securely erased before being returned. This not only protects the company, but also protects you since it is possible for personal information to exist on these machines. Reformatting a computer is not sufficient in these cases because it is possible for sensitive data to be recovered after reinstalling an operating system.

## Other Resources

### Okta

In an effort to secure access to systems, GitLab is utilizing Okta. The key goals are:

- We can use Okta to enable Zero-Trust based authentication controls upon our assets, so that we can allow authorized connections to key assets with a greater degree of certainty.
- We can better manage the login process to the 80+ and growing cloud applications that we use within our tech stack.
- We can better manage the Provisioning and De-provisioning process for our users to access these application, by use of automation and integration into our HRIS system.
- We can make Trust and Risk based decisions on authentication requirements to key assets, and adapt these to ensure a consistent user experience.

To read more about Okta, please visit the [Okta](https://about.gitlab.com/handbook/business-ops/okta/) page of the handbook.
