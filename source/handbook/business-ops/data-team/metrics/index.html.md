---
layout: markdown_page
title: "KPI Definition Links"
---

## On this page
{:.no_toc}

- TOC
{:toc}

This page is meant to help map a KPI to where it is defined in the handbook. The Data Team is responsible for maintaining this page.

## GitLab KPIs

1. [IACV](/handbook/finance/operating-metrics/#incremental-annual-contract-value-iacv) vs. plan > 1
1. TCV - OpEx vs. plan > 1
1. [Sales efficiency ratio](/handbook/finance/operating-metrics/#sales-efficiency-ratio) > 1
1. Pipe generated vs. plan > 1
1. Wider community contributions per release
1. [LTV / CAC](/handbook/finance/operating-metrics/#ltv-to-cac-ratio) ratio > 4
1. Average NPS
1. Hires vs. plan > 0.9
1. Monthly employee turnover
1. New hire average score
1. Merge Requests per release per developer
1. Uptime GitLab.com
1. Active users per hosting platform: Total, AWS, Azure, GCP, IBM, Unknown
1. [Support CSAT](handbook/finance/operating-metrics/#csat)
1. Runway > 12 months
1. [MAUI](http://www.meltano.com/docs/roadmap.html#maui) (Meltano so not part of the GitLab Executive Team KPIs) > 10% WoW

## Sales KPIs

1. [IACV](/handbook/finance/operating-metrics/#incremental-annual-contract-value-iacv) vs. plan > 1
1. [Field efficiency ratio](/handbook/finance/operating-metrics/#field-efficiency-ratio) > 2
1. [TCV](/handbook/finance/operating-metrics/#total-contract-value-tcv) vs. plan > 1
1. [ARR](/handbook/finance/operating-metrics/#annual-recurring-revenue-arr) YoY > 190%
1. Win rate > 30%
1. % of ramped reps at or above quota > 0.7
1. [Net Retention](/handbook/finance/operating-metrics/#retention-gross--net-dollar-weighted) > 2
1. [Gross Retention](/handbook/finance/operating-metrics/#retention-gross--net-dollar-weighted) > 0.9
1. Rep IACV per comp > 5
1. [ProServe](/handbook/finance/operating-metrics/#pcv) revenue vs. cost > 1.1
1. Services attach rate for strategic > 0.8
1. Self-serve sales ratio > 0.3
1. Licensed users
1. [ARPU](/handbook/finance/operating-metrics/#arpu)
1. New strategic accounts
1. [IACV per Rep](/handbook/finance/operating-metrics/#iacv-rep) > $1.0M
1. New hire location factor < TBD

## Marketing KPIs

1. Pipe generated vs. plan > 1
1. Pipe-to-spend > 5
1. [Marketing efficiency ratio](/handbook/finance/operating-metrics/#marketing-efficiency-ratio) > 2
1. SCLAU
1. [LTV / CAC ratio](/handbook/finance/operating-metrics/#ltv-to-cac-ratio) > 4
1. Twitter mentions
1. Sessions on our marketing site
1. New users
1. Product Installations: Download, start of installation, success installation, created admin user, configured email, second user invited, 30 day active, updates
1. Social response time
1. Meetup Participants with GitLab presentation
1. GitLab presentations given
1. Wider community contributions per release
1. Monthly Active Contributors from the wider community
1. New hire location factor < TBD

## People Operations KPIs

1. Hires vs. plan > 0.9
1. Apply to hire days < 30
1. No offer [NPS](/handbook/finance/operating-metrics/#nps) > [4.1](https://stripe.com/atlas/guides/scaling-eng)
1. Offer acceptance rate > 0.9
1. Average [NPS](/handbook/finance/operating-metrics/#nps)
1. Average location factor
1. New hire location factor < TBD
1. 12 month employee turnover < 16%
1. Voluntary employee turnover < 10%
1. Candidates per vacancy
1. Percentage of vacancies with active sourcing
1. New hire average score
1. Onboarding [NPS](/handbook/finance/operating-metrics/#nps)
1. Diversity lifecycle: applications, recruited, interviews, offers, acceptance, retention
1. PeopleOps cost per employee
1. [Discretionary bonus](/handbook/incentives/#discretionary-bonuses) per employee per month > 0.1

## Finance KPIs

1. [IACV](/handbook/finance/operating-metrics/#incremental-annual-contract-value-iacv) per [capital consumed](/handbook/finance/operating-metrics/#capital-consumption) > 2
1. [Sales efficiency](/handbook/finance/operating-metrics/#sales-efficiency-ratio) > 1.0
1. [Magic number](/handbook/finance/operating-metrics/#magic-number) > 1.1
1. [Gross margin](/handbook/finance/operating-metrics/gross-margin) > 0.9
1. [Average days of sales outstanding](/handbook/finance/operating-metrics/#days-sales-outstanding-dso) < 45
1. [Average days to close](/handbook/finance/operating-metrics/#days-to-close) < 10
1. Runway > 12 months
1. New hire location factor < 0.7
1. ARR by annual cohort
1. Reasons for churn
1. Reasons for net expansion
1. Refunds processed as % of orders


## Product KPIs

1. [Ambition Percentage](/direction/#how-we-plan-releases) (Merged Issues/Planned Issues per Release) ~ 70%
1. SMAU
1. [MAU](/handbook/finance/operating-metrics/#monthly-active-user-mau)
1. Stage Monthly Active Instances (SMAI)
1. Monthly Active Instances (MAI)
1. [Sessions on release post](/handbook/finance/operating-metrics/#sessions-release-post)
1. Installation churn
1. User churn
1. Lost Instances
1. [Acquisition](/direction/growth/#acquistion)
1. [Adoption](/direction/growth/#adoption)
1. [Upsell](/direction/growth/#upsell)
1. [Rention](/direction/growth/#retention)
1. Net Promotor Score / [Customer Satisfaction](/handbook/finance/operating-metrics/#csat) with the product
1. New hire location factor < TBD
1. Signups
1. Onboarding completion rate
1. Comments
1. Accepting Merge Requests issue growth

## Engineering KPIs

1. Merge Requests per release per engineer in product development > 10
1. Uptime GitLab.com > 99.95%
1. Performance GitLab.com
1. Support [SLA](/handbook/finance/operating-metrics/#sla)
1. Support [CSAT](/handbook/finance/operating-metrics/#csat)
1. Support cost vs. recurring revenue
1. Days to fix S1 security issues
1. Days to fix S2 security issues
1. Days to fix S3 security issues
1. GitLab.com infrastructure cost per MAU
1. [ARR](/handbook/finance/operating-metrics/#annual-recurring-revenue-arr) per support rep > $1.175M
1. New hire location factor < 0.5
1. Public Cloud Spend

## Alliances KPIs

1. Active users per hosting platform: Total, AWS, Azure, GCP, IBM, Unknown
1. Active installations per hosting platform: Total, AWS, Azure, GCP, IBM, Unknown
1. Product Downloads: Updates & Initial per distribution method: Omnibus, Cloud native helm chart, Source
1. Acquisition velocity: [Acquire 3 teams per quarter](/handbook/alliances/acquisition-offer/) for less than $2m in total.
1. Acquisition success: 70% of acquisitions ship the majority of their old product functionality as part of GitLab within 3 months after acquisition.invited, 30 day active, updates


## All other metrics
Many metrics are important for tracking progress, but are not the top KPIs for the organization.

* [Average days to close](link to location in handbook)
* [Average Sales Price (ASP)](link to location in handbook)
* [Capital Consumption](link to location in handbook)
* [Cash Burn, Average Cash Burn and Runway](link to location in handbook)
* [Contract Value](link to location in handbook)
   * [Annual Contract Value (ACV)](link to location in handbook)
   * [Incremental Annual Contract Value (IACV)](link to location in handbook)
   * [Gross Incremental Annual Contract Value (Gross IACV)](link to location in handbook)
   * [Growth Incremental Annual Contract Value (Growth IACV)](link to location in handbook)
   * [New Incremental Annual Contract Value (New IACV)](link to location in handbook)
   * [ProServe Contract Value (PCV)](link to location in handbook)
   * [Total Contract Value (TCV)](link to location in handbook)
* [Cost per MQL](link to location in handbook)
* [Credit](link to location in handbook)
* [Customer Satisfaction (CSAT)](link to location in handbook)
* [Customers](link to location in handbook)
   * [Customer Segmentation](link to location in handbook)
   * [Customer Counts](link to location in handbook)
* [Customer Acquisition Cost (CAC)](link to location in handbook)
* [Customer Acquisition Cost (CAC) Ratio](link to location in handbook)
* [Days Sales Outstanding (DSO)](link to location in handbook)
* [Downgrade](link to location in handbook)
* [Field efficiency ratio](link to location in handbook)
* [Free Cash Flow (FCF)](link to location in handbook)
* [GitLab.com User and Group Churn](link to location in handbook)
* [Gross Burn Rate](link to location in handbook)
* [Gross Margin](link to location in handbook)
* [Licensed Users](link to location in handbook)
* [Life-Time Value (LTV)](link to location in handbook)
* [Life-Time Value to Customer Acquisition Cost Ratio (LTV:CAC)](link to location in handbook)
* [Lost instances](link to location in handbook)
* [Lost Renewal](link to location in handbook)
* [Magic Number](link to location in handbook)
* [Marketing efficiency ratio](link to location in handbook)
* [Marketo Qualified Lead (MQL)][Customer lifecycle](/handbook/business-ops/#customer-lifecycle)
* [Monthly Active Group (MAG)](link to location in handbook)
* [Monthly Active User (MAU)](link to location in handbook)
* [New ACV / New Customers](link to location in handbook)
* [New ACV / New Customers by Sales Assisted](link to location in handbook)
* [Non GAAP Revenue (Ratable Recognition)](link to location in handbook)
* [Reasons for Churn / Expansion, Dollar Weighted](link to location in handbook)
* [Retention, Gross & Net (Dollar Weighted)](link to location in handbook)
* [Revenue](link to location in handbook)
   * [Annual Recurring Revenue (ARR)](link to location in handbook)
   * [ARR by Annual Cohort](link to location in handbook)
   * [Monthly Recurring Revenue (MRR)](link to location in handbook)
* [Revenue per Licensed User (also known as ARPU)](link to location in handbook)
* [Sales Efficiency Ratio](link to location in handbook)
* [Sales Qualified Lead (SQL)](link to location in handbook)
* [Rep Productivity](link to location in handbook)
* [Service Level Agreement (SLA)](link to location in handbook)
* [Social Response Time](link to location in handbook)
