---
layout: markdown_page
title: "Recruiting Process - Sourcer Tasks"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Recruiting Process Framework - Sourcer Tasks
{: #framework-source}

### Step 8/S: Source top talent, schedule screens

Source passive applicants through job boards, such as LinkedIn or Indeed. If passive applicant is responsive, import applicant into Greenhouse. Schedule a screening call between the passive applicant and the recruiter.
