---
layout: markdown_page
title: "Manage Team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Manage
{: #welcome}

The responsibilities of this team are described by the [Manage product
category](/handbook/product/categories/#manage). Among other things, this means 
working on GitLab's functionality around user, group and project administration, 
authentication, access control, and subscriptions.

* I have a question. Who do I ask?

In GitLab issues, questions should start by @ mentioning the Product Manager for the [Manage product
category](/handbook/product/categories/#dev). GitLabbers can also use [#g_manage](https://gitlab.slack.com/messages/CBFCUM0RX).

### How we work

* In accordance with our [GitLab values](https://about.gitlab.com/handbook/values/)
* Transparently: nearly everything is public, we record/livestream meetings whenever possible
* We get a chance to work on the things we want to work on
* Everyone can contribute; no silos

#### Planning

We plan in monthly cycles in accordance with our [Product Development Timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline). 
Release scope for an upcoming release should be finalized by the `1st`. We use the following timeline:

* On or around the `24th`: Product meets with Engineering Managers for a preliminary issue review. Issues are tagged with a milestone and are ready for [estimation](handbook/product/categories/manage/index.html#estimation).
   * Issues will be pre-assigned to team members after this preliminary review for estimation. Most issues should be estimated by the `1st`.
* On or around the `28th`: Product takes the reviewed scope to the larger team for discussion. This becomes the release scope for the next release.
* On or around the `6th`: Product meets with Engineering Managers for a pre-kickoff check-in.

#### Prioritization

Our planning process is reflected in boards that should always reflect our current priorities. You can see our priorities for [backend](https://gitlab.com/groups/gitlab-org/-/boards/704240) and [frontend](https://gitlab.com/groups/gitlab-org/-/boards/810974) engineering. Please filter these boards for the relevant milestone.

 Priorities should be reflected in the priority label for scheduled issues:

| Priority | Description | Probability of shipping in milestone | 
| ------ | ------ | ------ |
| P1 | **Urgent**: top priority for achieving in the given milestone. These issues are the most important goals for a release and should be worked on first; some may be time-critical or unblock dependencies. | ~100% |
| P2 | **High**: important issues that have significant positive impact to the business or technical debt. Important, but not time-critical or blocking others.  | ~75% |
| P3 | **Normal**: incremental improvements to existing features. These are important iterations, but deemed non-critical. | ~50% |
| P4 | **Low**: stretch issues that are acceptable to postpone into a future release. | ~25% | 

You can read more about prioritization on the [direction page](https://about.gitlab.com/direction/manage/#how-we-prioritize) for the Manage stage of the DevOps lifecycle.

#### Estimation

Before work can begin on an issue, we should estimate it first after a preliminary investigation. 

Separate issues should be created for each discipline that's involved (see [an example](https://gitlab.com/gitlab-org/gitlab-ee/issues/9288)), and scheduled issues without a weight should be assigned the "estimation:needed" label.

When estimating development work, please assign an issue an appropriate weight:

| Weight | Description (Engineering) |
| ------ | ------ |
| 1 | The simplest possible change. We are confident there will be no side effects. |
| 2 | A simple change (minimal code changes), where we understand all of the requirements. | 
| 3 | A simple change, but the code footprint is bigger (e.g. lots of different files, or tests effected). The requirements are clear. | 
| 5 | A more complex change that will impact multiple areas of the codebase, there may also be some refactoring involved. Requirements are understood but you feel there are likely to be some gaps along the way. | 
| 8 | A complex change, that will involve much of the codebase or will require lots of input from others to determine the requirements. | 
| 13 | A significant change that may have dependencies (other teams or third-parties) and we likely still don't understand all of the requirements. It's unlikely we would commit to this in a milestone, and the preference would be to further clarify requirements and/or break in to smaller Issues. | 

For UX issues, please use a similar methodology:

| Weight | Description (UX) |
| ------ | ------ |
| 1 | Mostly small to medium UI changes, smaller UX improvements, without unanswered questions |
| 2 | Simple UI or UX change where we understand all of the requirements, but may need to find solutions to known questions/problems. | 
| 3 | A simple change but the scope of work is bigger (lots of UI or UX changes/improvements required). Multiple pages are involved, we're starting to design/redesign small flows. Some unknown questions may arise during the work. | 
| 5 | A complex change where other team members will need to be involved. Spans across multiple pages, we're working on medium-sized flows. There are significant open questions that need to be answered. | 
| 8 | A complex change that spans across large flows and may require input from other designers. This is the largest flow design/redesign that we would take on in a single milestone. | 
| 13 | A significant change that spans across multiple flows and that would require significant input from others (teams, team members, user feedback) and there are many unknown unknowns. It's unlikely we would commit to this in a milestone, and the preference would be to further clarify requirements and/or break in to smaller Issues. | 

As part of estimation, if you feel the issue is in an appropriate state for an engineer to start working on it, please add the ~"workflow::ready for development" label. Alternatively, if there are still requirements to be defined or questions to be answered that you feel an engineer won't be able to easily resolve, please add the ~blocked label. Issues with the ~blocked label will appear in their own column on our planning board ([example](https://gitlab.com/groups/gitlab-org/-/boards/1012536)), making it clear that they need further attention.

Once an issue has been estimated, the ~"estimation:needed" label can be removed.

#### During the release

If a developer has completed work on an issue, they may open the prioritization board for their respective area ([backend](https://gitlab.com/groups/gitlab-org/-/boards/704240) or [frontend](https://gitlab.com/groups/gitlab-org/-/boards/810974))
and begin working on the next prioritized issue. 

Issues not already in development should be worked on in priority order.

#### Retrospectives

After the `8th`, the Manage team conducts an [asynchronous retrospective](https://about.gitlab.com/handbook/engineering/management/team-retrospectives/). You can find current and past retrospectives for Manage in [https://gitlab.com/gl-retrospectives/manage/issues/](https://gitlab.com/gl-retrospectives/manage/issues/).

### Links and resources
{: #links}

* Our repository
  * A number of our team discussions and issues are located at [`gitlab-org/manage`](https://gitlab.com/gitlab-org/manage/)
* Our Slack channel
  * [#g_manage](https://gitlab.slack.com/messages/CBFCUM0RX)
* Calendar
  * GitLabbers can add [this Manage team calendar](https://calendar.google.com/calendar/b/1?cid=Z2l0bGFiLmNvbV9rOWYyN2lqamExaGoxNzZvbmNuMWU4cXF2a0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t) in Google Calendar.
* Meeting agendas
  * Agendas and notes from team meetings can be found in [this Google Doc](https://docs.google.com/document/d/1kE8udlwjAiMjZW4p1yARUPNmBgHYReK4Ks5xOJW6Tdw/edit). For transparency across the team, we use one agenda document.
* Recorded meetings
  * Recordings should be added to YouTube and added to [this playlist](https://www.youtube.com/playlist?list=PLFGfElNsQthZ-D0khZ_NSb5Bdl2xkF97m).
